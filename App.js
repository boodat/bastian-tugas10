import React from 'react';
import {
  SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, useColorScheme, View,
} from 'react-native';

import Routing from './src/navigation/Routing';


const App = ({Navigation, route}) => {
  return (
    // <View style={{backgroundColor: 'red'}} >
    //   {/* <StatusBar barStyle = "dark-content"/>
    //   <Routing/> */}
    // </View>

    <SafeAreaView style={{flex: 1,}}>
      <Routing/>
  </SafeAreaView>
    
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
