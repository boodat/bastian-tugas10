import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AuthNavigation from './AuthNavigation'

const Stack = createStackNavigator();

function Routing () {
    return (
        <NavigationContainer >
<Stack.Navigator screenOptions={{headerShown: false}}>
    <Stack.Screen name='AuthNavigation' component={AuthNavigation}/>
</Stack.Navigator>
</NavigationContainer>
    )
}
export default Routing;
