import React, {useState} from 'react'
import { View, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native'
import {Colors} from '../../styles'
import { NumberFormatter } from '../../utils/Helper'
import Icon from 'react-native-vector-icons/AntDesign'
import ModalBottom from '../../component/modal/ModalBottom'
import ModalCenter from '../../component/modal/ModalCenter'
import {TextRegular, TextBold, TextMedium, Header, InputText} from '../../component/global'

const Login = ({navigation, route}) => {
    const [modalBottom, setModalBottom] = useState(false)
   const [modalCenter, setModalCenter] = useState(false)

    return (
        <View style={{flex: 1, backgroundColor: '#fff',}}>

            <Header
            title='LOGIN'
            onPressBack={() => navigation.goBack()}
            isBoldTitle={true}
            titleSize={18}
            backgroundHeader={Colors.PRIMARY}
            titleColor={Colors.WHITE}
            iconBackColor={Colors.WHITE}/>
            <View style={{margin: 10}}>
            {/* <Text>
                Login Screen
            </Text> */}

            <InputText style={{
                widht: '90%',
                alignItems: 'center',
                marginTop: 20,
            }}
            placeholderText='Masukkan Email'
            keyboardType='email-address'/>
             <InputText style={{
                widht: '90%',
                alignItems: 'center',
                marginTop: 20,
            }}
            placeholderText='Masukkan Nomor Telepon'
            keyboardType='number-pad'/>
             <InputText style={{
                widht: '90%',
                alignItems: 'center',
                marginTop: 20,
            }}
            placeholderText='Masukkan Password'
            isPassword={true}/>
            </View>
            

<View>
           <TouchableOpacity
               style={styles.btnShowModalBottom}
               onPress={()=> setModalBottom(true)}
           >
               <TextBold
                   text="Modal Bottom"
                   size={16}
                   color="#fff"
               />
           </TouchableOpacity>
           <TouchableOpacity
               style={[styles.btnShowModalBottom, {backgroundColor: Colors.DEEPORANGE}]}
           
               onPress={()=> setModalCenter(true)}>
               <TextBold
                   text="Modal Center"
                   size={16}
                   color="#fff"
               />
           </TouchableOpacity>
       </View>

       <ModalBottom show={modalBottom}
       onClose={()=> setModalBottom(false)}
       title='Modal Bottom'>
        <View>
        <TextRegular text='Ini Text Reguler'
            color='#000'/>
            <TextMedium text='Ini Text Medium'
            color='#000'/>
            <TextBold text='Ini Text Bold' 
            color='#000'
            style={{marginTop: 20,}}/>
        </View>
       </ModalBottom>

       <ModalCenter show={modalCenter}
       onClose={()=> setModalCenter(false)}
       title='Modal Center'>
        <View>
        <TextRegular text='Ini Text Reguler'
            color='#000'/>
            <TextMedium text='Ini Text Medium'
            color='#000'/>
            <TextBold text='Ini Text Bold' 
            color='#000'
            style={{marginTop: 20,}}/>

            <TextRegular
            text={`${NumberFormatter(100000, 'Rp. ')}`}
            size={17}
            color={Colors.BLACK}
            style={{
                marginTop: 20,
                marginLeft: 20
            }} />
        </View>
       </ModalCenter>

        </View>
    )
}

export default Login;

const styles = StyleSheet.create({
    btnShowModalBottom: {
        width: '35%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10,
        borderRadius: 6,
        backgroundColor: Colors.PRIMARY,
        alignSelf: 'center',
        marginTop: 20
    }
 })
 